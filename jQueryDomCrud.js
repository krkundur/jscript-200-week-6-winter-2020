
$( document ).ready(function() {
// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>

const $pTags = $('p');
console.log($pTags[$pTags.length -1]);


// Access (read) the data-color attribute of the <img>,
// log to the console

const $img = $('img');
console.log("Attribute is "+$img.attr('data-color'));

// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"

const $li = $('li');
console.log($li[2]);
$li.addClass('highlight');
// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
$pTags[$pTags.length-1].remove();

// Create a listener on the "Buy Now!" link that responds to a click event.
// When clicked, the the "Buy Now!" link should be removed
// and replaced with text that says "Added to cart"
$('<a>',{
    text: 'Buy Now',
    title: 'Blah',
    id: 'cta',
    href: '#',
    click: function(){ $('a').remove();console.log(this);$('body'). append("<div>Added to cart</div>");}
}).appendTo('body');
});
