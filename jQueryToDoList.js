$( document ).ready(function() {
/**
 * Toggles "done" class on <li> element
 */

 
$('li').click(function(e) {
  let $this = $(this);
  $this.toggleClass('done');
  });

/**
 * Delete element when delete link clicked
 */
$('a').click(function(e) {
if($(this).attr("class") == "delete") {
  console.log("here in delete");
  $(this).parents('li').remove();
}
});

/**
 * Adds new list item to <ul>
 */
const addListItem = function(e) {
  e.preventDefault();
  const text = $('input').val();
  
  let $newLi = $('<li>');
  let $newSpan = $('<span>');
  let $newA = $('<a class="delete">Delete</a>');
  $newSpan.text(text);
  $newLi.append($newSpan);
  $newLi.append($newA);
  $(document).on('click','li',function(e) {
    let $this = $(this);
    $this.toggleClass('done');
    });
  $(document).on('click','a',function(e) {
    if($(this).attr("class") == "delete") {
      console.log("here in delete");
      $(this).parents('li').remove();
    }
    });
    
  $('ul').prepend($newLi);

};

$('a').click(function(e) {
  if($(this).attr("class") == "add-item") {
    addListItem(e);
  }
  });
// add listener for add
});
